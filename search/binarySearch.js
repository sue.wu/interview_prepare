// 二分查找法
function ArrayList() {
	let array = [];
	this.insert = function(item) {
		array.push(item);
	};
	// 方便輸出結果
	this.toString = function() {
		return array.join();
	};
  this.binarySearch = function(item, startLength, maxLength) {
		if(maxLength == -1){
			maxLength = array.length;
		}
		console.log("當前array: start位置為"+startLength+", max長度位置為"+maxLength);
        let result = -1;
        let mid = Math.floor(maxLength/2);
        if(startLength == maxLength){
            mid = startLength;
        }
        if(startLength != 0){
            mid = startLength+(Math.floor((maxLength-startLength)/2));
        }
        console.log("找出"+item+"在哪? 目前中間值(下標:"+mid+"))為:"+array[mid]);
		if(maxLength > 0){
	        console.log(array[mid]+" == "+item+":"+(array[mid] == item));
            if(array[mid] == item) {
                result = mid+1;
                return result;
            }

            else if(item < array[mid]){
                maxLength = mid; 
                return this.binarySearch(item, startLength, maxLength);
            }else if(item > array[mid]){
                startLength = mid; 
                return this.binarySearch(item, startLength, maxLength);
            }
        }
        return result;
  };
}

function createNonSortedArray(size) {
	let array = new ArrayList();
	for(let i = 1; i < size+1; i++) {
		array.insert(i);
	}
	console.log("當前array: "+array.toString());
	return array;
}

console.log("二分查找法規則：從數組長度/2且去除上一個中間數為來獲取中間數，如果數組長度為偶數，如果該目標下標值比對不相等(x==n)則會比較比對值(x)是否大於目標下標值(n)，如果大於(x>n)則取後半段的數組範圍優先比對，則取下標(數組長度/2)+1的目標下標來做比對，如果小於(x<n)擇取前半段的數組範圍優先比對則取下標(數組長度/2)-1的目標下標來做比對，: \n");
let array = createNonSortedArray(10);
let index = array.binarySearch(1, 0, -1);
console.log("array.binarySearch() return "+index);
console.log("運行結束。");
console.log("答案:目標在第"+index+"個數組下標");
