// 線性查找法
function ArrayList() {
	let array = [];
	this.insert = function(item) {
		array.push(item);
	};
	// 方便輸出結果
	this.toString = function() {
		return array.join();
	};
  this.linearSearch = function(item) {
		console.log("從小排到大的過程: ");
		let length = array.length;
		let result = -1;
		for(let i = 0; i < length; i++) {

	      console.log("第"+(i)+"次比較:");
				console.log("array["+i+"] == "+item+": "+ (array[i]==item));
  			if(array[i] == item) {
						result = i+1;
						break;
  			}
				console.log("\n");
		}
		return result;
  };

  // nextOne = function(index1, index2) {
  // 	continue;
  // };
}

function createNonSortedArray(size) {
	let array = new ArrayList();
	for(let i = 1; i < size+1; i++) {
		array.insert(i);
	}
	console.log("當前array: "+array.toString());
	return array;
}

let array = createNonSortedArray(5);
let index = array.linearSearch(3);
console.log("運行結束。");
console.log("答案:目標在第"+index+"個數組下標");
