// 插入排序法
function ArrayList() {
	let array = [];
	this.insert = function(item) {
		array.push(item);
	};
	// 方便輸出結果
	this.toString = function() {
		return array.join();
    };
    this.insertSort = function() {
		console.log("從小排到大的過程: ");
        let length = array.length;
		for(let unSortNum = 0; unSortNum < length; unSortNum++) {
            console.log("第"+(unSortNum+1)+"次比較排序，當前array: "+array.toString());
            console.log("當前比較基數為: "+array[unSortNum]);

            for(let sortedNum = 0; sortedNum < unSortNum; sortedNum++)
            {   
                console.log("比較是否 未排序值("+array[unSortNum]+") < 已排序值("+array[sortedNum]+")嗎？ "+(array[unSortNum] < array[sortedNum]));
                if(array[unSortNum] < array[sortedNum]) {
                    console.log("找到比基數("+array[unSortNum]+")更大數已排序值: "+array[sortedNum]);
                    insert(sortedNum, unSortNum);
                    break;
                }
            }
            console.log("\n");
        }
  };

  const insert = function(replaceIndex, insertIndex) {
    // console.log("before replaceIndex: "+replaceIndex);
    // console.log("before insertIndex: "+insertIndex);
    // 把新增目標位置值插入到取代位置，原該取代位置的值將會自動往後排序
    array.splice(replaceIndex, 0, array[insertIndex]);
    // 刪掉舊新增目標位置值
    console.log("把("+(insertIndex+1)+")位置插入到("+replaceIndex+")，當前array: "+array);
    array.splice(insertIndex+1, 1);
    console.log("刪除位置("+(insertIndex+1)+")後 當前array: "+array);
  };
}

function createNonSortedArray(size) {
    let array = new ArrayList();
    // 會輸出n個隨機排序的數字
	for(let i = size; i > 0; i--) {
        let num = Math.floor(Math.random() * size)+1;
        array.insert(num);
    }
	console.log("原本array: "+array.toString());
	return array;
}

let array = createNonSortedArray(5);
array.insertSort();
console.log("運行結束。");
console.log("答案: "+array.toString());
