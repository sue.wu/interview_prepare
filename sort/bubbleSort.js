// 氣泡排序法
function ArrayList() {
	let array = [];
	this.insert = function(item) {
		array.push(item);
	};
	// 方便輸出結果
	this.toString = function() {
		return array.join();
	};
  this.bubbleSort = function() {
		console.log("從小排到大的過程: ");
		let length = array.length;
		for(let i = 0; i < length; i++) {
  		// 比到後來最後一位已經排序好
      console.log("i的第"+(i+1)+"個數("+array[i]+") 當前array="+ array.toString());

  		for(let j = 0; j < length - 1; j++) {
	      console.log("j的第"+(j+1)+"次比較:");
				console.log("array["+j+"]:"+ array[j]);
				console.log("array["+(j+1)+"]:"+ array[j+ 1]);
  			if(array[j] > array[j + 1]) {
  				swap(j, j + 1);
  			}
  		}
			console.log("\n");
  	}
		
  };

  const swap = function(index1, index2) {
  	let tmp = array[index1];
  	array[index1] = array[index2];
  	array[index2] = tmp;
  };
}

function createNonSortedArray(size) {
	let array = new ArrayList();
	for(let i = size; i > 0; i--) {
		array.insert(i);
	}
	console.log("原本array: "+array.toString());
	return array;
}

let array = createNonSortedArray(5);
array.bubbleSort();
console.log("運行結束。");
console.log("答案: "+array.toString());
