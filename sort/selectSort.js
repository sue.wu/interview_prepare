// 選擇排序法
function ArrayList() {
	let array = [];
	this.insert = function(item) {
		array.push(item);
	};
	// 方便輸出結果
	this.toString = function() {
		return array.join();
	};
  this.selectSort = function() {
		console.log("從小排到大的過程: ");
        let length = array.length;
		for(let baseMin = 0; baseMin < length; baseMin++) {
            console.log("第"+(baseMin+1)+"次排序，當前array: "+array.toString());
            console.log("當前比較基數為: "+array[baseMin]);

            for(let num = baseMin; num < length; num++)
            {   
                if(array[baseMin] > array[num]) {
                    console.log("找到比基數("+array[baseMin]+")更小數: "+array[num]);
                    swap(baseMin, num);
                }
            }
            console.log("\n");
        }
  };

  const swap = function(index1, index2) {
  	let tmp = array[index1];
  	array[index1] = array[index2];
  	array[index2] = tmp;
  };
}

function createNonSortedArray(size) {
	let array = new ArrayList();
	for(let i = size; i > 0; i--) {
		array.insert(i);
	}
	console.log("原本array: "+array.toString());
	return array;
}

let array = createNonSortedArray(5);
array.selectSort();
console.log("運行結束。");
console.log("答案: "+array.toString());
