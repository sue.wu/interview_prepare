// 合併排序法
function ArrayList() {
    let array = [];
    let newArray = [];
	this.insert = function(item) {
		array.push(item);
	};
	// 方便輸出結果
	this.toString = function() {
		return array.join();
	};
  this.selectSort = function() {
		console.log("從小排到大的過程: ");
        console.log("先切開array至最小1個單位的過程: ");
        //先切開array至最小1個單位
		array = spliteArray(array, "init", 0);
  };

  const spliteArray = function(tmpArray, part, splitTimes) {
    let length = tmpArray.length;
    splitTimes++;
    if(length == 1){
        return tmpArray;
    }
    const mid = Math.floor(length / 2);
    console.log("第"+splitTimes+"層 tmpArray("+tmpArray+") 中位長度數: "+mid);
    const left = tmpArray.slice(0, mid);
    console.log("第"+splitTimes+"層 left組: "+left);

    const right = tmpArray.slice(mid, length);
    console.log("第"+splitTimes+"層 right組: "+right);

    return merge(spliteArray(left, "left", splitTimes), spliteArray(right, "right", splitTimes));
  };

  const merge = function(left, right) {
    console.log(" ");
    console.log("合併開始: left["+left.join()+"] + right["+right.join()+"]");

    // let temp = newArray.concat(left, right);
    const result = [];
    let iLeft = 0;
    let iRight = 0;
    
    while(iLeft < left.length && iRight < right.length) {
        console.log("當 if(left["+iLeft+"] < right["+iRight+"]): "+(left[iLeft] < right[iRight]));
        if(left[iLeft] < right[iRight]) {
            result.push(left[iLeft++]);
            console.log("所以放 left的 "+result[result.length-1]+" 到 result array");
        } else {
            result.push(right[iRight++]);
            console.log("所以放 right的 "+result[result.length-1]+" 到 result array");
        }
        console.log("目前result array: "+result.join());
    }

    while(iLeft < left.length) {
        // console.log("while(iLeft < left.length)="+(iLeft < left.length)+" before, result: "+result.join());
        result.push(left[iLeft++]);
        console.log("放入剩下的 left array: "+left.toString());
    }

    
    while(iRight < right.length) {
        // console.log("while(iRight < right.length)="+(iRight < right.length)+" before, result: "+result.join());
        result.push(right[iRight++]);
        console.log("放入剩下的 right array: "+right.toString());
    }
    
    return result;
  };

  const swap = function(index1, index2) {
  	let tmp = array[index1];
  	array[index1] = array[index2];
  	array[index2] = tmp;
  };
}

// 線性循序的結果
// function createNonSortedArray(size) {
// 	let array = new ArrayList();
// 	for(let i = size; i > 0; i--) {
// 		array.insert(i);
// 	}
// 	console.log("原本array: "+array.toString());
// 	return array;
// }

// 亂數排序的結果
function createNonSortedArray(size) {
    let array = new ArrayList();
    // 會輸出n個隨機排序的數字
	for(let i = size; i > 0; i--) {
        let num = Math.floor(Math.random() * size)+1;
        array.insert(num);
    }
	console.log("原本array: "+array.toString());
	return array;
}

let array = createNonSortedArray(8);
array.selectSort();
console.log("運行結束。");
console.log("答案: "+array.toString());
